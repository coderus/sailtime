# SailTime

SailTime is a simple uptime program for Sailfish OS, written in C++, QML and
Javascript. It shows the user the uptime of the device, along with the system
load averages (1, 5 and 15 minutes) and some additional miscellaneous
information. It also has an automatic update timer with a configurable interval
and can save the system's all-time uptime record.

SailTime is available in the Jolla app store.

## Licence

SailTime is open source, licensed under the MIT Expat licence. See the LICENCE
file for more details.

© Mikko Ahlroth 2013–2014

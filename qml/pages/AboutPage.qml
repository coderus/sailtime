/*
 * © Mikko Ahlroth 2013–2014
 * SailTime is open source software. For licensing information, please check
 * the LICENCE file.
 */

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: aboutPage

    SilicaFlickable {
        anchors.fill: parent

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: "About"
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: "SailTime 1.2.0"
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeLarge
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: "© Mikko Ahlroth 2013–2014"
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeMedium
            }

            Separator {
                x: Theme.paddingLarge
                width: parent.width - Theme.paddingLarge * 2
                horizontalAlignment: Qt.AlignCenter
                color: Theme.highlightColor
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: "SailTime is open source software. You can find the licence details and source code on BitBucket."
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeMedium
                wrapMode: Text.WordWrap
                width: parent.width - Theme.paddingLarge * 2
            }

            Button {
                text: "Open project website in browser"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: Qt.openUrlExternally("https://bitbucket.org/Nicd/sailtime");
            }

            Separator {
                x: Theme.paddingLarge
                width: parent.width - Theme.paddingLarge * 2
                horizontalAlignment: Qt.AlignCenter
                color: Theme.highlightColor
            }

            PageHeader {
                title: "What are load averages?"
            }


            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: "In UNIX computing, the system load is a measure of the amount of \
computational work that a computer system performs. The load average represents the average \
system load over a period of time. It conventionally appears in the form of three numbers \
which represent the system load during the last one-, five-, and fifteen-minute periods."
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeMedium
                wrapMode: Text.WordWrap
                width: parent.width - Theme.paddingLarge * 2
            }

            Button {
                text: "Open Wikipedia description"
                anchors.horizontalCenter: parent.horizontalCenter
                onClicked: Qt.openUrlExternally("http://en.wikipedia.org/wiki/Load_average");
            }
        }
    }
}



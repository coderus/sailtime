/*
 * © Mikko Ahlroth 2013–2014
 * SailTime is open source software. For licensing information, please check
 * the LICENCE file.
 */

#include <QtQuick>
#include <QtQml>

#include <sailfishapp.h>

#include "uptimechecker.h"
#include "sysinfoc.h"
#include "sysload.h"


int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/template.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //
    // To display the view, call "show()" (will show fullscreen on device).

    // Register needed metatypes
    qmlRegisterType<UptimeChecker>("harbour.sailtime.UptimeChecker", 2, 0, "UptimeChecker");
    qmlRegisterUncreatableType<SysinfoC>("harbour.sailtime.SysinfoC", 1, 0, "SysinfoC", "Cannot create sysinfo");
    qmlRegisterUncreatableType<Sysload>("harbour.sailtime.Sysload", 1, 0, "Sysload", "Cannot create sysload");

    return SailfishApp::main(argc, argv);
}


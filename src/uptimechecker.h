/*
 * © Mikko Ahlroth 2013–2014
 * SailTime is open source software. For licensing information, please check
 * the LICENCE file.
 */

#ifndef UPTIMECHECKER_H
#define UPTIMECHECKER_H

#include <QObject>
#include <sys/sysinfo.h>
#include "sysinfoc.h"

class UptimeChecker : public QObject
{
    Q_OBJECT
public:
    explicit UptimeChecker(QObject *parent = 0);

    Q_INVOKABLE SysinfoC* fetchUptime();
};

#endif // UPTIMECHECKER_H

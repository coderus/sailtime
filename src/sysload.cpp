#include "sysload.h"

Sysload::Sysload(float loads[3], QObject *parent) :
    QObject(parent),
    avg_1(loads[0]),
    avg_5(loads[1]),
    avg_15(loads[2])
{
}

float Sysload::getAvg1() const
{
    return avg_1;
}

float Sysload::getAvg5() const
{
    return avg_5;
}

float Sysload::getAvg15() const
{
    return avg_15;
}
